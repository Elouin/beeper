require "file_utils"
require "ini"
require "option_parser"

CONF_PATHS = [FileUtils.pwd, "#{Path.home}/.config"]
conf : Hash(String, Hash(String, String)) = {} of String => Hash(String, String)
beepfile : String = ""
stdout : Bool = false

parser = OptionParser.new do |parser|
  parser.banner = "Usage: beeper <message> [arguments]"
  parser.on("-h", "--help", "Show this help") do
    puts parser
    exit
  end
  parser.on("-f PATH", "--beeptext-file=PATH", "beeptext file location") { |_path| beepfile = _path }
  parser.on("-s","--stdout", "print to stdout instead of writing to file") { stdout = true }
  parser.invalid_option do |flag|
    STDERR.puts "ERROR: #{flag} is not a valid option."
    STDERR.puts parser
    exit(1)
  end
end

parser.parse

CONF_PATHS.each do |path|
  begin
    conf = INI.parse(File.open("#{path}/beeper.conf"))
  rescue File::NotFoundError
    next
  end
end

if beepfile == ""
  if conf == {} of String => Hash(String, String)
    conf["default"] = {} of String => String
    puts "No config file found."
    puts "Location of beeptxt file: "
    conf["default"]["beeptxt_location"] = gets.to_s
  elsif !conf["default"]["beeptxt_location"]
    puts "Config file does not contain beeptxtfile location."
    puts "Location of beeptxt file: "
    conf["default"]["beeptxt_location"] = gets.to_s
  end
  unless conf["default"]["beeptxt_location"].starts_with?('/')
    conf["default"]["beeptxt_location"] = "#{FileUtils.pwd}/#{conf["default"]["beeptxt_location"]}"
  end
  File.write("#{Path.home}/.config/beeper.conf", INI.build(conf))
  beepfile = conf["default"]["beeptxt_location"]
end

if stdout
  puts "#{`date -Is`.to_s.strip} #{ARGV.join(" ")}\n"
  exit
end

File.write(beepfile, "#{`date -Is`.to_s.strip} #{ARGV.join(" ")}\n", mode: "a+")
